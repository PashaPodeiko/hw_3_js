/*
Теоретический вопрос
1. Описать своими словами для чего вообще нужны функции в программировании.
2. Описать своими словами, зачем в функцию передавать аргумент.


Теоретический ответ
1. Функции нужны для вызова какого-то кода один или несколько раз.
2.	Функция нуждается в аргументах, если она обрабатывает разные переменные в основном коде JS или возвращает результат работы над аргументами.

Задание
Реализовать функцию, которая будет производить математические операции с введеными пользователем числами.

Технические требования:
- Считать с помощью модального окна браузера два числа.
- Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено +, -, *, /.
- Создать функцию, в которую передать два значения и операцию.
- Вывести в консоль результат выполнения функции.

*/

let firstNumber = prompt("Enter number 1");

while (numberIsInvalid(firstNumber)) {
	firstNumber = prompt("Enter number 1");
}

let mahtOperator = prompt("Enter maht operation", "+, -, *, /");

let secondNumber = prompt("Enter number 2");

while (numberIsInvalid(secondNumber)) {
	secondNumber = prompt("Enter number 2");
}

function caculate(element1, element2, func) {
	let result = null;
	if (func === "+") {
		result = element1 + element2;
	} else if (func === "-") {
		result = element1 - element2;
	} else if (func === "*") {
		result = element1 * element2;
	} else if (func === "/") {
		result = element1 / element2;
	}
	return result;
}

function numberIsInvalid(value) {
	return Number.isNaN(+value) || value === null || value === "";
}

console.log(caculate(+firstNumber, +secondNumber, mahtOperator));



